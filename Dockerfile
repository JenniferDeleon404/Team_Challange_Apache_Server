#Getting base image
FROM ubuntu:latest
#running the latest version of Apache server
RUN apt update
RUN apt install apache2 -y
#changing the name of the file
COPY ./index.html /var/www/html
#Setting location to run in
EXPOSE 80
#Start apache2 automatically in a ubuntu docker container
CMD ["apachectl", "-D", "FOREGROUND"]