//# Team Challange Apache Server.

You are tasked to create an HTTPD server, however you cannot utilize the base image of HTTPD from the dockerhub. Instead:

[X] your base image will be an Ubuntu image.

[X] Install Apache 2 (which is the same as HTTPD depending on OS) onto the Ubuntu Image.

[X] Change the index.html file located /var/www/html directory. In order to change the file, it will be required to copy the file as part of your installation instructions.

[X] Should be able to successfully hit the endpoints of the VM and see your update index.html (the same as which was used to SSH).

This challange will help the understanding of core concepts. 
